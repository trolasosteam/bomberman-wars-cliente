package com.ziroudev.game;

import java.io.IOException;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.EndPoint;
import com.esotericsoftware.kryonet.Listener;

public class Network extends Listener{
	
	Client client;

	public void connect() throws IOException{
		client = new Client();
		register(client);
		client.addListener(this);
		
		client.start();
		
		client.connect(2000, "localhost", 6000, 6000);
	}
	
	public void register (EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		kryo.register(Mapa.class);
		kryo.register(Object[][].class);
		kryo.register(Object[].class);
		kryo.register(Player.class);
		kryo.register(PacketAddPlayer.class);
		kryo.register(MovePlayer.class);
	}
	
	public void received(Connection conn, Object ob){
		if(ob instanceof PacketAddPlayer){
			System.out.println("CLIENTE: recivo addPlayer");
			PacketAddPlayer packet = (PacketAddPlayer) ob;
			Player newPlayer = new Player();
			Cliente.playerList.put(packet.id, newPlayer);
			System.out.println("CLIENTE: a�ado player ID "+packet.id);
		}else if(ob instanceof MovePlayer){
			System.out.println("CLIENTE: recivo MovePlayer");
			MovePlayer packet = (MovePlayer) ob;
			Cliente.playerList.get(packet.id).posX = packet.posX;System.out.println("player:"+packet.id+"posX"+Cliente.playerList.get(packet.id).posX);
			Cliente.playerList.get(packet.id).posY = packet.posY;
		}
	}

	
}