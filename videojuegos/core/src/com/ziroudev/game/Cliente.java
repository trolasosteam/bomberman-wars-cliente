 package com.ziroudev.game;
import java.io.IOException;
import java.util.HashMap;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Cliente extends ApplicationAdapter {
	//variables de pantalla
	int pantallaWidth, pantallaHeight;
	int pantallaPosX, pantallaPosY;
	
	//vars de conexion
	Network network;
	Player player;
	static HashMap<Integer, Player> playerList = new HashMap<Integer, Player>();
	
	//variables de libgdx
	SpriteBatch batch;
	Texture PlayerTexture, PlayerTexture2, background;
	Sprite pj;
	Mapa map;
	
	@Override
	public void create () {
		pantallaHeight = Gdx.graphics.getHeight();
		pantallaWidth = Gdx.graphics.getWidth();
		//pantallaPosX = pantallaWidth / 10;
		//pantallaPosY = pantallaHeight / 10;
		try {
			network = new Network();
			network.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("CLIENTE: conectado correctamente...");
		
		
		
		batch = new SpriteBatch();
		map = new Mapa();
		map.creaMapa();
		map.muestraMapa();
		player = new Player();

		
		//textures y sprites
		PlayerTexture = new Texture("pj.png");
		PlayerTexture2 = new Texture("pj2.png");
		background = new Texture("bg.png");
		pj = new Sprite(PlayerTexture);
	}
	
	
	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//INICIO RENDER
		batch.begin();
		
	
		batch.draw(background, 0, 0, pantallaWidth, pantallaHeight);
		//dibujo en pantalla el player
		pj.setBounds(player.posX, player.posY, 30, 30);
		pj.draw(batch);
		player.input();
		update();
		
		//dibujo en pantalla los otros players de la lista
		for(Player myplayer : playerList.values()){
			Sprite pj = new Sprite(PlayerTexture2);
			pj.setBounds(myplayer.posX, myplayer.posY, 30, 30);
			pj.draw(batch);
		}
			
		
		//FIN RENDER
		batch.end();
	}
	
	public void update(){
		//compruebo la sincronizacion de posiciones
		if(player.networkPosX != player.posX || player.networkPosY != player.posY){
			MovePlayer packet = new MovePlayer();
			packet.posX = player.posX;
			packet.posY = player.posY;
			network.client.sendUDP(packet);
			
			//sincronizo la netposX con la posX
			player.networkPosX = player.posX;
			player.networkPosY = player.posY;
		}
	}
	
}
